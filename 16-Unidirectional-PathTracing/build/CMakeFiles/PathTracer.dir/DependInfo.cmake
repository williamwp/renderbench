
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/wangpeng/tools/3d/203/PathTracer/src/Main.cpp" "CMakeFiles/PathTracer.dir/src/Main.cpp.o" "gcc" "CMakeFiles/PathTracer.dir/src/Main.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/wangpeng/tools/3d/203/PathTracer/build/src/CMakeFiles/Core.dir/DependInfo.cmake"
  "/home/wangpeng/tools/3d/203/PathTracer/build/external/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "/home/wangpeng/tools/3d/203/PathTracer/build/external/glad/CMakeFiles/glad.dir/DependInfo.cmake"
  "/home/wangpeng/tools/3d/203/PathTracer/build/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/wangpeng/tools/3d/203/PathTracer/build/external/imguizmo/CMakeFiles/imguizmo.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
