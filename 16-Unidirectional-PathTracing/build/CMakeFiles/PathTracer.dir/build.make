# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.20

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /home/wangpeng/tools/3d/49/cmake-3.20.0-rc3/bin/cmake

# The command to remove a file.
RM = /home/wangpeng/tools/3d/49/cmake-3.20.0-rc3/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/wangpeng/tools/3d/203/PathTracer

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/wangpeng/tools/3d/203/PathTracer/build

# Include any dependencies generated for this target.
include CMakeFiles/PathTracer.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/PathTracer.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/PathTracer.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/PathTracer.dir/flags.make

CMakeFiles/PathTracer.dir/src/Main.cpp.o: CMakeFiles/PathTracer.dir/flags.make
CMakeFiles/PathTracer.dir/src/Main.cpp.o: ../src/Main.cpp
CMakeFiles/PathTracer.dir/src/Main.cpp.o: CMakeFiles/PathTracer.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/wangpeng/tools/3d/203/PathTracer/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/PathTracer.dir/src/Main.cpp.o"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -MD -MT CMakeFiles/PathTracer.dir/src/Main.cpp.o -MF CMakeFiles/PathTracer.dir/src/Main.cpp.o.d -o CMakeFiles/PathTracer.dir/src/Main.cpp.o -c /home/wangpeng/tools/3d/203/PathTracer/src/Main.cpp

CMakeFiles/PathTracer.dir/src/Main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/PathTracer.dir/src/Main.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/wangpeng/tools/3d/203/PathTracer/src/Main.cpp > CMakeFiles/PathTracer.dir/src/Main.cpp.i

CMakeFiles/PathTracer.dir/src/Main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/PathTracer.dir/src/Main.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/wangpeng/tools/3d/203/PathTracer/src/Main.cpp -o CMakeFiles/PathTracer.dir/src/Main.cpp.s

# Object files for target PathTracer
PathTracer_OBJECTS = \
"CMakeFiles/PathTracer.dir/src/Main.cpp.o"

# External object files for target PathTracer
PathTracer_EXTERNAL_OBJECTS =

PathTracer: CMakeFiles/PathTracer.dir/src/Main.cpp.o
PathTracer: CMakeFiles/PathTracer.dir/build.make
PathTracer: src/libCore.a
PathTracer: external/glfw/src/libglfw3.a
PathTracer: external/glad/libglad.a
PathTracer: external/imgui/libimgui.a
PathTracer: external/imguizmo/libimguizmo.a
PathTracer: /usr/lib/x86_64-linux-gnu/librt.so
PathTracer: /usr/lib/x86_64-linux-gnu/libm.so
PathTracer: CMakeFiles/PathTracer.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/wangpeng/tools/3d/203/PathTracer/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable PathTracer"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/PathTracer.dir/link.txt --verbose=$(VERBOSE)
	/home/wangpeng/tools/3d/49/cmake-3.20.0-rc3/bin/cmake -E copy_directory /home/wangpeng/tools/3d/203/PathTracer/assets /home/wangpeng/tools/3d/203/PathTracer/build/assets/
	/home/wangpeng/tools/3d/49/cmake-3.20.0-rc3/bin/cmake -E copy_directory /home/wangpeng/tools/3d/203/PathTracer/shaders /home/wangpeng/tools/3d/203/PathTracer/build/shaders/

# Rule to build all files generated by this target.
CMakeFiles/PathTracer.dir/build: PathTracer
.PHONY : CMakeFiles/PathTracer.dir/build

CMakeFiles/PathTracer.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/PathTracer.dir/cmake_clean.cmake
.PHONY : CMakeFiles/PathTracer.dir/clean

CMakeFiles/PathTracer.dir/depend:
	cd /home/wangpeng/tools/3d/203/PathTracer/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/wangpeng/tools/3d/203/PathTracer /home/wangpeng/tools/3d/203/PathTracer /home/wangpeng/tools/3d/203/PathTracer/build /home/wangpeng/tools/3d/203/PathTracer/build /home/wangpeng/tools/3d/203/PathTracer/build/CMakeFiles/PathTracer.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/PathTracer.dir/depend

