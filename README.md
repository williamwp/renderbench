# RenderBench

RenderBench is a benchmark for GPU rendering programs, including rasterization, ray tracing, photo mapping and so on, which is written in CUDA.

**Rendering Arithmatic Supported**

| name    | description             | status                    |
| ------- | ---------------- | ----------------------- |
| ray tracing    | info         | :ballot_box_with_check: |
| path tracing  | info     | :ballot_box_with_check: |
| fractal | info | :ballot_box_with_check: |
| photon-mapping  | info     | :ballot_box_with_check: |


